/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.odev2;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Scanner;

/**
 *
 * @author abdurrahman_sefer
 */
public class main_class {

    //C:\\Users\\Abdurrahman_Sefer\\Documents\\NetBeansProjects\\odev2\\target\\classes\\com.mycompany.odev2.meyvesuyu_class
    //com.mycompany.odev2.meyvesuyu_class
    public static void main(String[] args) throws MalformedURLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        
        
       
        
        
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Lutfen Siniflarin adresini giriniz : ");
        String classPath = sc.nextLine();
        System.out.println();
        System.out.println("Lutfen acmak istediginiz sinifin ismini giriniz : ");
        String className = sc.nextLine();
        System.out.println();
        System.out.println();                  
        //Aldığımız adres ve sınıfı ismi ile oluşturduğumuz MyInterpreter sınıfın loadClass metoduna gönderdik ve gelen sınıfın ismini aldık
        String s = MyInterpreter.loadClass(classPath, className).toString();
        
        
        System.out.println(s);       
        System.out.println();
        
        
        //gelen sınıf ata sınıfa cust ettik ve yazdir() metodundan gelen sonucu ekrana yazdırdık
        lokanta class_ = (lokanta) MyInterpreter.loadClass(classPath, className).newInstance();
        System.out.println(class_.yazdir());

    }
}
