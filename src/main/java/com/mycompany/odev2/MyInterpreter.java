/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.odev2;

import java.net.URL;
import java.net.URLClassLoader;

/**
 *
 * @author abdurrahman_sefer
 */
public class MyInterpreter {

    public static Class loadClass(String path, String name) {
        try {

            URLClassLoader urlClassLoader = URLClassLoader.newInstance(new URL[]{
                new URL(
                "file:///" + path
                )
            });

            Class clazz = urlClassLoader.loadClass(name);
            return clazz;

        } catch (Exception ex) {
            String error = ex.getMessage();
        }

        return null;
    }

}
